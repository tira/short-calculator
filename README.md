# One Line Calculator
A very short calculator program.

totals at 3 lines, and 218 bytes.

## Compilation

nix:

    g++ onl.cpp -o onl

windows (cc):

    i686-w64-mingw32-g++ onl.cpp -o onl.exe --static
    
--static increases the file size to over 9MB, probably will be smaller if you natively compile on Windows (untested)
